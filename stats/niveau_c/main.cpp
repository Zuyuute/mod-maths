#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define nbcol 10

using namespace std;

int rdtsc()
{
    __asm__ __volatile__("rdtsc");
}

void direction(float tab[][nbcol], int tablo[5], int cas){

    for(int i=0; i<cas; i++){
        cout << "Combien il y a de population dans le cas " << i << " ? " << endl;
        cin >> tablo[i];
    }
    int tablointer[5]={0,0,0,0,0};
    int compteur;
    float d;
    float alea;
    int nb;
    cout << "Combien de repetition ?" << endl;
    cin >> nb;
    float total = tab[0][0];

    for (int k=0; k<nb; k++){                  // BOUCLE POUR LES DIFFERENTS JOURS

        for(int j=0; j<cas; j++){          // BOUCLE POUR LES DIFFERENTES POP

            for(int i=0; i<tablo[j]; i++){    // BOUCLE POUR FAIRE LE CHEMIN SELON LE TABLEAU DE POP

                compteur=-1;
                total=0;
                srand(rdtsc());
                d=rand();
                alea=(float)(d/RAND_MAX);
                while (total <= alea){
                    compteur++;
                    total = total + tab[j][compteur];
                }

                //cout << "je suis dans le cas " << j << " et je l'ai fait " << i << endl;

                //cout << "x = " << j+1 <<  " T = " << i+1 << " : " << compteur << endl;

            //cout << compteur << endl;

            switch(compteur){
                    case 0:
                        tablointer[0]++;
                        break;
                    case 1:
                        tablointer[1]++;
                        break;
                    case 2:
                        tablointer[2]++;
                        break;
                    case 3:
                        tablointer[3]++;
                        break;
                    case 4:
                        tablointer[4]++;
                        break;
            }

            }

        }

        for(int i=0;i<cas;i++){
            tablo[i]=tablointer[i];
            tablointer[i] = 0;
            cout << "Pour le temps T = "  << k+1 << " Il y a une population dans " << i << " de : " << tablo[i] << endl;

        }
        cout << endl << endl;


    }
}


int main()
{
    int tablo[5]={0,0,0,0,0};
    int pop;
    //cout << "Combien de pop de base ?" << endl;
    //cin >> pop;
//rajouter le contenu d'un fichier de configuration
int nbcas = 2;
float tab[nbcas][nbcol];

tab[0][0]= 0.7;
tab[0][1]= 0.3;

tab[1][0]= 0.9;
tab[1][1]= 0.1;

    direction(tab, tablo, nbcas);
    cout << endl;

    for(int i=0; i<nbcas; i++){
       cout << "Le systeme finit " << tablo[i] << " fois dans le cas " << i << endl;
    }
    cout << endl << endl;
    return 0;
}
