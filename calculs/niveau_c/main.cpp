#include <iostream>
#include<stdio.h>
#include<stdlib.h>
#define nbcol 10

using namespace std;


////////////////////////////////////////////////////////////////////////////////////////
//////////////////// SOUS-PROGRAMME PROBABILITE TOTALES ////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
float proba_tot(int *nb, float tab[][nbcol], float init[], int t, string tabc[]){
    float probtot[t][*nb];
    float somme;


        //cout << "Probabilite totale a t = 0" << endl;   //POUR T=0
        for (int j=0; j<*nb; j++){
            probtot[0][j] = init[j];
            //cout << "Probabilite totale du cas " << j << " = " << probtot[0][j] << endl;
        }


        for(int i=1; i<t; i++){     //BOUCLE DU TEMPS
            cout << endl << "Probabilite totale a t = " << i ;

            for (int j=0; j<*nb; j++){      //BOUCLE DES EVENEMENTS
                somme = 0;
                for (int k=0; k<*nb; k++){
                    somme = somme + probtot[i-1][k]*tab[k][j];
                }
                probtot[i][j] = somme;
                cout << endl << "P(" << j << ") = " << probtot[i][j] << "  (" << tabc[j] << ")";
            }

            cout << endl;
        }


}




int main()
{

    ////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////// INSERTION DES DONNEES ////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////

int nbcas= 4;
float tab[nbcas][nbcol];

string tableaucorrespondance[nbcas];
tableaucorrespondance[0] = "dans la salle Nord'";
tableaucorrespondance[1] = "dans la salle Est";
tableaucorrespondance[2] = "dans la salle Ouest";
tableaucorrespondance[3] = "dans la salle Sud";

tab[0][0]= 0;
tab[0][1]= 0.5;
tab[0][2]= 0.5;
tab[0][3]= 0;

tab[1][0]= 0.5;
tab[1][1]= 0;
tab[1][2]= 0;
tab[1][3]= 0.5;

tab[2][0]= 0.5;
tab[2][1]= 0;
tab[2][2]= 0;
tab[2][3]= 0.5;

tab[3][0]= 0;
tab[3][1]= 0.5;
tab[3][2]= 0.5;
tab[3][3]= 0;

    //////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////// VARIABLES //////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////

    int t;

    cout << "Combien de temps voulez-vous passer ?" << endl;
    cin >> t;


    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////// DECLARATION DES PROBABILITES INITIALES //////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////

    float probinit[nbcas];

    probinit[0] = 1;
    probinit[1] = 0;
    probinit[2] = 0;
    //probinit[3] = 0.01;
    //probinit[4] = 0.01;



    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////// PROBABILITE TOTALE //////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////

    float totale = proba_tot(&nbcas, tab, probinit, t, tableaucorrespondance);


}
